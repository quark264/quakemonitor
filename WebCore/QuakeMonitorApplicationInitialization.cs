﻿using System.Linq;
using DatabaseModelCore;
using DatabaseModelCore.Context;
using DatabaseModelCore.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace WebCore
{
    public static class QuakeMonitorApplicationInitialization
    {
        public static void InitializeQuakeMonitorApplication(this IApplicationBuilder builder)
        {
            using (var scope = builder.ApplicationServices.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                var unitOfWork = serviceProvider.GetRequiredService<UnitOfWork>();

                if (unitOfWork.QuakeEvents.GetAll().Any())
                {
                    return;
                }

                DbInitializer.InitializeDb(serviceProvider.GetRequiredService<QuakeMonitorContext>());
            }
        }
    }
}