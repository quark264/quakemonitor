﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace WebCore
{
    public sealed class Program
    {
        public static void Main()
        {
            new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
#if DEBUG
                .UseIISIntegration()
#endif
                .UseStartup<Startup>()
                .ConfigureLogging((context, logging) =>
                {
                    var logLevel = LogLevel.Warning;

                    logging.AddConsole().SetMinimumLevel(logLevel);
                    logging.AddDebug().SetMinimumLevel(logLevel);
                })
                .Build()
                .Run();
        }
    }
}
