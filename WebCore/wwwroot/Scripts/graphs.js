﻿var distribution = $("#distribution-chart");
var beniof = $("#beniof-chart");

$.ajax({
    url: "api/graph/distributionGraph",
    type: "GET",
    dataType: "json",
    success: function(data) {
        var result = data;
        PrepareData(result);
    },
    error: function(x, y, z) {
        alert(x + "\n" + y + "\n" + z);
    }
});

function PrepareData(items) {
    var months = items.map(e => e.Month + " " + e.Year);
    var counts = items.map(e => e.EventsCount);
    var lgEs = items.map(e => e.LgE);
    CreateDistributionGraph(months, counts, lgEs);
    CreateBeniofGraph(months, lgEs);
}

function CreateDistributionGraph(months, counts, lgEs) {
    var options = {
        scales: {
            yAxes: [
                {
                    id: "Count",
                    type: "linear",
                    position: "left",
                    scaleLabel: {
                        display: true,
                        labelString: "Nz",
                        fontColor: "rgba(66, 170, 255, 0.5)",
                        fontSize: 16,
                        fontStyle: "bold"
                    },
                    ticks: {
                        fontSize: 14
                    }
                },
                {
                    id: "Lg E",
                    type: "linear",
                    position: "right",
                    scaleLabel: {
                        display: true,
                        labelString: "LgE",
                        fontColor: "rgba(0, 0, 0, 1)",
                        fontSize: 16,
                        fontStyle: "bold"
                    },
                    ticks: {
                        fontSize: 14
                    }
                }
            ],
            xAxes: [
                {
                    id: "Months",
                    ticks: {
                        fontSize: 14
                    }
                }
            ]
        }
    };
    var distributionChart = new Chart(distribution,
        {
            type: "bar",
            data: {
                labels: months,
                datasets: [
                    {
                        label: "Count",
                        yAxisID: "Count",
                        data: counts,
                        backgroundColor: "rgba(66, 170, 255, 0.5)",
                        borderColor: "rgba(66, 170, 255, 0.5)"
                    },
                    {
                        label: "Lg E",
                        yAxisID: "Lg E",
                        data: lgEs,
                        type: "line",
                        backgroundColor: "rgba(0, 0, 0, 0)",
                        borderColor: "rgba(0, 0, 0, 1)"
                    }
                ]
            },
            options: options
        });
    distributionChart.canvas.parentNode.style.height = 1;
}

function CreateBeniofGraph(months, lgEs) {
    var Es = lgEs.map(e => Math.pow(10, e));
    var summedEs = Es.map((e, i) => Es.slice(0, i + 1).reduce((a, b) => a + b));
    var options = {
        scales: {
            yAxes: [
                {
                    id: "E",
                    scaleLabel: {
                        display: true,
                        labelString: "E",
                        fontColor: "rgba(0, 0, 0, 1)",
                        fontSize: 16,
                        fontStyle: "bold"
                    },
                    ticks: {
                        fontSize: 14
                    }
                }
            ],
            xAxes: [
                {
                    id: "Months",
                    ticks: {
                        fontSize: 14
                    }
                }
            ]
        }
    };
    var beniofChart = new Chart(beniof,
        {
            type: "line",
            data: {
                labels: months,
                datasets: [
                    {
                        label: "E",
                        data: summedEs,
                        backgroundColor: "rgba(0, 0, 0, 0)",
                        borderColor: "rgba(0, 0, 0, 1)"
                    }
                ]
            },
            options: options
        });
    beniofChart.canvas.parentNode.style.height = 1;
}

$("#distribution-chart a").click(function (e) {
    e.preventDefault();
    $(this).tab("show");
});

$("#beniof-chart a").click(function (e) {
    e.preventDefault();
    $(this).tab("show");
});