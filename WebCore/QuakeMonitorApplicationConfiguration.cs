﻿using DatabaseModelCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace WebCore
{
    public static class QuakeMonitorApplicationConfiguration
    {
        public static IServiceCollection AddQuakeMonitorApplication(this IServiceCollection collection)
        {
            collection.AddQuakeMonitorPersistance();
            collection.AddQuakeMonitorAppInteractors();

            return collection;
        }

        public static IApplicationBuilder UseQuakeMonitorApplication(this IApplicationBuilder builder)
        {
            builder.UseQuakeMonitorPersistance();
            builder.InitializeQuakeMonitorApplication();

            return builder;
        }

        private static void AddQuakeMonitorAppInteractors(this IServiceCollection collection)
        {
            //TODO: Add interactors here
        }
    }
}