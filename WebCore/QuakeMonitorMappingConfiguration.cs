﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace WebCore
{
    public static class QuakeMonitorMappingConfiguration
    {
        public static IServiceCollection AddQuakeMonitorServiceMapping(this IServiceCollection collection)
        {
            collection.AddSingleton<IMapper>(new Mapper(
                new MapperConfiguration(conf => conf.AddQuakeMonitorServiceMapperProfiles())
            ));

            return collection;
        }

        private static IMapperConfigurationExpression AddQuakeMonitorServiceMapperProfiles(
            this IMapperConfigurationExpression configuration)
        {
            //TODO: Add mapping profiles here

            return configuration;
        }
    }
}