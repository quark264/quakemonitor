﻿using Microsoft.AspNetCore.Mvc;

namespace WebCore.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Title"] = "Map Page";

            return View();
        }

        public ActionResult Graph()
        {
            ViewBag["Title"] = "Graph Page";

            return View();
        }
    }
}
