﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DatabaseModelCore.Repository;
using DomainEntitiesCore;
using Microsoft.AspNetCore.Mvc;
using WebCore.Dto;

namespace WebCore.Controllers
{
    public class SeismicController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public static readonly string Name = AspRoutes.GetControllerName<SeismicController>();

        public const string Events = "events";

        public const string Activities = "activities";

        [HttpGet]
        [ActionName(Events)]
        //[Route("api/seismic/GetQuakeEvents/")]
        public IEnumerable<QuakeEventDto> GetQuakeEvents(DateTime? startDate, DateTime? endDate)
        {
            var config = new MapperConfiguration(n => n.CreateMap<QuakeEvent, QuakeEventDto>());
            config.AssertConfigurationIsValid();
            var mapper = config.CreateMapper();
            IEnumerable<QuakeEvent> events;
            if (startDate != null
                && endDate != null)
            {
                events = _unitOfWork.QuakeEvents.GetAll()
                    .Where(e => e.Date >= startDate && e.Date < endDate)
                    .ToArray();
            }
            else
            {
                events = _unitOfWork.QuakeEvents.GetAll().ToArray();
            }

            return mapper.Map<IEnumerable<QuakeEventDto>>(events).ToList();
        }

        [HttpGet]
        [ActionName(Activities)]
        //[Route("api/seismic/GetSeismicActivities/")]
        public IEnumerable<SeismicActivityDto> GetSeismicActivities()
        {
            var events = _unitOfWork.QuakeEvents.GetAll().ToArray();
            var config = new MapperConfiguration(n => n.CreateMap<SeismicActivity, SeismicActivityDto>());
            config.AssertConfigurationIsValid();
            var mapper = config.CreateMapper();
            if (events.All(e => !e.IsNew))
            {
                return mapper.Map<IEnumerable<SeismicActivityDto>>(_unitOfWork.SeismicActivities.GetAll());
            }

            const double startLat = 70.0;
            const double startLng = -10.0;
            var currentLat = startLat;
            var currentLng = startLng;
            var rightBorder = 70.0;
            var startYear = events.Select(e => e.Date.Year).Min();
            var seismicActivities = _unitOfWork.SeismicActivities.GetAll().ToArray();
            _unitOfWork.Save();
            var activities = new List<SeismicActivityDto>();
            while (currentLat <= 90)
            {
                if (currentLat > 75)
                {
                    rightBorder = 100;
                }

                while (currentLng <= rightBorder)
                {
                    var eventsCount = events.Count(
                        e =>
                            e.Longitude >= currentLng
                            && e.Latitude >= currentLat
                            && e.Longitude <= (currentLng + KmToLngDegrees(30, currentLat))
                            && e.Latitude <= (currentLat + KmToLatDegrees(30)));
                    if (eventsCount == 0)
                    {
                        currentLng += KmToLngDegrees(30, currentLat);

                        continue;
                    }

                    activities.Add(
                        new SeismicActivityDto
                        {
                            Latitude = currentLat + KmToLatDegrees(15),
                            Longitude = currentLng + KmToLngDegrees(15, currentLat),
                            Activity = CalculateActivity(eventsCount, startYear)
                        });
                    if (seismicActivities.Length != 0)
                    {
                        var seismicActivity = seismicActivities.FirstOrDefault(
                            n =>
                                n.Latitude == currentLat + KmToLatDegrees(15)
                                && n.Longitude == currentLng + KmToLngDegrees(15, currentLat));
                        if (seismicActivity != null)
                        {
                            seismicActivity.Activity = CalculateActivity(eventsCount, startYear);
                            _unitOfWork.SeismicActivities.Edit(seismicActivity);
                        }
                        else
                        {
                            _unitOfWork.SeismicActivities.Create(
                                new SeismicActivity
                                {
                                    Latitude = currentLat + KmToLatDegrees(15),
                                    Longitude = currentLng + KmToLngDegrees(15, currentLat),
                                    Activity = CalculateActivity(eventsCount, startYear)
                                });
                        }
                    }
                    else
                    {
                        _unitOfWork.SeismicActivities.Create(
                            new SeismicActivity
                            {
                                Latitude = currentLat + KmToLatDegrees(15),
                                Longitude = currentLng + KmToLngDegrees(15, currentLat),
                                Activity = CalculateActivity(eventsCount, startYear)
                            });
                    }

                    _unitOfWork.Save();
                    currentLng += KmToLngDegrees(30, currentLat);
                }

                currentLng = startLng;
                currentLat += KmToLatDegrees(30);
            }

            UpdateEvents();

            return activities;
        }

        [HttpPost]
        //[Route("api/seismic/CreateQuakeEvent")]
        public void CreateQuakeEvent([FromBody] QuakeEventDto model)
        {
            var config = new MapperConfiguration(n => n.CreateMap<QuakeEventDto, QuakeEvent>());
            config.AssertConfigurationIsValid();
            var mapper = config.CreateMapper();
            _unitOfWork.QuakeEvents.Create(mapper.Map<QuakeEvent>(model));
        }

        private void UpdateEvents()
        {
            const double startLat = 70.0;
            const double startLng = -10.0;
            var rightBorder = 70.0;
            var currentLat = startLat;
            var currentLng = startLng;
            var events = _unitOfWork.QuakeEvents.GetAll().ToArray();
            var seismicActivities = _unitOfWork.SeismicActivities.GetAll().ToArray();
            while (currentLat <= 90)
            {
                if (currentLat > 75)
                {
                    rightBorder = 100;
                }

                while (currentLng <= rightBorder)
                {
                    var filtredEvents = events.Where(
                            n =>
                                n.Longitude >= currentLng
                                && n.Latitude >= currentLat
                                && n.Longitude <= (currentLng + KmToLngDegrees(30, currentLat))
                                && n.Latitude <= (currentLat + KmToLatDegrees(30)))
                        .ToArray();
                    if (filtredEvents.Length == 0)
                    {
                        currentLng += KmToLngDegrees(30, currentLat);

                        continue;
                    }

                    var seismicActivity = seismicActivities.FirstOrDefault(
                        n =>
                            n.Latitude == currentLat + KmToLatDegrees(15)
                            && n.Longitude == currentLng + KmToLngDegrees(15, currentLat));
                    foreach (var quakeEvent in filtredEvents.Where(n => n.IsNew))
                    {
                        quakeEvent.SeismicActivityId = seismicActivity.Id;
                        quakeEvent.IsNew = false;
                        _unitOfWork.QuakeEvents.Edit(quakeEvent);
                    }

                    _unitOfWork.Save();
                    currentLng += KmToLngDegrees(30, currentLat);
                }

                currentLng = startLng;
                currentLat += KmToLatDegrees(30);
            }

            var unproccessedEvents = _unitOfWork.QuakeEvents.GetAll().Where(n => n.SeismicActivityId == null).ToArray();
            foreach (var unproccessedEvent in unproccessedEvents)
            {
                unproccessedEvent.IsNew = false;
                _unitOfWork.QuakeEvents.Edit(unproccessedEvent);
            }

            _unitOfWork.Save();
        }

        private static double CalculateActivity(int eventsCount, int startYear)
        {
            return ((1 - Math.Pow(10.0, -0.75)) / Math.Pow(10.0, -0.75 * (3.0 - 3.33)))
                   * ((1.0 * 1000) / ((DateTime.Now.Year - startYear) * 900))
                   * eventsCount;
        }

        private static double KmToLatDegrees(double km)
        {
            return km * 1000 * 180 / 20004274.0;
        }

        private static double KmToLngDegrees(double km, double lat)
        {
            return (km * 1000) / (111.32 * 1000 * Math.Cos(lat * (Math.PI / 180.0)));
        }
    }
}