﻿using System.Collections.Generic;
using System.Linq;
using DatabaseModelCore.Repository;
using Microsoft.AspNetCore.Mvc;
using WebCore.Dto;

namespace WebCore.Controllers
{
    public class GraphController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public static readonly string Name = AspRoutes.GetControllerName<GraphController>();

        public const string DistributionGraph = "distributionGraph";

        [HttpGet]
        [ActionName(DistributionGraph)]
        //[Route("Home/api/graph/GetDistributionGraph")] //make normal later
        public IEnumerable<DistributionGraphDto> GetDistributionGraph()
        {
            return _unitOfWork.QuakeEvents.GetAll()
                .GroupBy(e => e.Date.Year)
                .Select(
                    e => new
                    {
                        Year = e.Key,
                        Months = e.GroupBy(x => x.Date.ToString("MMMM"))
                            .Select(
                                x => new
                                {
                                    Name = x.Key,
                                    EventsCount = x.Count(),
                                    LgE = 4 + 1.8 * x.Max(o => o.Magnitude)
                                })
                            .ToList()
                    })
                .SelectMany(
                    e => e.Months,
                    (e, month) => new DistributionGraphDto
                    {
                        Year = e.Year,
                        Month = month.Name,
                        EventsCount = month.EventsCount,
                        LgE = month.LgE
                    });
        }
    }
}