﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using WebCore.Controllers;

namespace WebCore
{
    public static class QuakeMonitorApiRoutes
    {
        public const string ApiPrefix = "api";

        public static IRouteBuilder AddQuakeMonitorApiRoutes(this IRouteBuilder builder)
        {
            builder
                .MapQuakeMonitorRoute(
                    $"/{ApiPrefix}/{SeismicController.Name}/{SeismicController.Events}",
                    SeismicController.Name,
                    SeismicController.Events)
                .MapQuakeMonitorRoute(
                    $"/{ApiPrefix}/{SeismicController.Name}/{SeismicController.Activities}",
                    SeismicController.Name,
                    SeismicController.Activities)
                .MapQuakeMonitorRoute(
                    $"/{ApiPrefix}/{GraphController.Name}/{GraphController.DistributionGraph}",
                    GraphController.Name,
                    GraphController.DistributionGraph);

            return builder;
        }

        private static IRouteBuilder MapQuakeMonitorRoute(this IRouteBuilder builder, string template, string controllerName, string actionName)
        {
            return builder.MapRoute($"{controllerName}:{actionName}", template, AspRoutes.RouteValues(controllerName, actionName));
        }
    }
}