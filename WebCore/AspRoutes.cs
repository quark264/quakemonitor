﻿using System;
using Microsoft.AspNetCore.Routing;

namespace WebCore
{
    public static class AspRoutes
    {
        public const string Contoller = "controller";

        public const string Action = "action";

        public static string GetControllerName<T>()
        {
            var type = typeof(T);

            var typeName = type.Name;

            var controllerName = typeName;
            if (typeName.EndsWith(Contoller, StringComparison.OrdinalIgnoreCase))
            {
                controllerName = typeName.Substring(0, typeName.Length - Contoller.Length);
            }

            return controllerName;
        }

        public static RouteValueDictionary RouteValues(string controllerName = null, string actionName = null)
        {
            return new RouteValueDictionary()
                .With(Contoller, controllerName)
                .With(Action, actionName);
        }

        public static RouteValueDictionary With(this RouteValueDictionary dictionary, string paramName, object value)
        {
            if (value == null)
            {
                dictionary.Remove(paramName);
            }
            else
            {
                dictionary[paramName] = value;
            }

            return dictionary;
        }
    }
}