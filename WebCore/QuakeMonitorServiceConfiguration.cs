﻿using DatabaseModelCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebCore
{
    public static class QuakeMonitorServiceConfiguration
    {
        public static IServiceCollection AddQuakeMonitorService(this IServiceCollection collection)
        {
            collection.AddQuakeMonitorServiceMapping();
            collection.AddQuakeMonitorServiceConfiguration();

            collection.AddQuakeMonitorApplication();

            collection.AddCors();
            collection.AddMemoryCache();
            collection.AddSession();
            collection.AddMvc();

            return collection;
        }

        public static IApplicationBuilder UseQuakeMonitorService(this IApplicationBuilder builder)
        {
            builder.UseQuakeMonitorApplication();

            builder.UseCors(o => o
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            builder.UseDefaultFiles();
            builder.UseStaticFiles();
            builder.UseSession();
            builder.UseMvc(routes => routes.AddQuakeMonitorApiRoutes());

            return builder;
        }

        private static IServiceCollection AddQuakeMonitorServiceConfiguration(this IServiceCollection collection)
        {
            collection.AddSingleton(new ConfigurationBuilder().AddEnvironmentVariables().Build());
            collection.AddSingleton<IQuakeMonitorApplicationOptions, QuakeMonitorServiceOptions>();
            collection.AddSingleton<IQuakeMonitorDatabaseModelOptions>(e =>
                e.GetRequiredService<IQuakeMonitorApplicationOptions>());

            return collection;
        }
    }
}