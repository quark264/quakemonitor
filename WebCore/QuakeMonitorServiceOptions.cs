﻿using Microsoft.Extensions.Configuration;

namespace WebCore
{
    public class QuakeMonitorServiceOptions : IQuakeMonitorApplicationOptions
    {
        private const string KeyQuakeMonitorDbConnection = "QUAKE_MONITOR_DB_CONNECTION";
        //private const string KeyQuakeMonitorUser = "QUAKE_MONITOR_USER";
        //private const string KeyQuakeMonitorPassword = "QUAKE_MONITOR_PASSWORD";

        private readonly IConfigurationRoot _configuration;

        public string GetConnectionString() => _configuration[KeyQuakeMonitorDbConnection];

        //public string GetAdminUserName() => _configuration[KeyQuakeMonitorUser];

        //public string GetAdminUserPassword() => _configuration[KeyQuakeMonitorPassword];

        public QuakeMonitorServiceOptions(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }
    }
}