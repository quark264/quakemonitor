﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using DomainEntities;

namespace DatabaseModel
{
    public class DbInitializer : CreateDatabaseIfNotExists<QuakeContext>
    {
        protected override void Seed(QuakeContext context)
        {
            const string path = "G:\\Documents\\Visual Studio 2017\\Projects\\QuakeMonitor\\землетрясения+.txt";
            //const string path = "C:\\Users\\Tom\\Documents\\Visual Studio 2017\\Projects\\QuakeMonitor\\землетрясения+.txt";
            context.QuakeEvents.AddRange(ParseEventsFromFile(path));
            base.Seed(context);
        }

        private static IEnumerable<QuakeEvent> ParseEventsFromFile(string path)
        {
            var strr = new StreamReader(new FileStream(path, FileMode.Open));
            var result = strr.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var strings = result.Skip(1).Take(result.Length - 1).ToArray();
            return strings.Select(str => str.Split(' ')).Select(fields => new QuakeEvent
            {
                Date = DateTime.ParseExact($"{fields[0]} {fields[1]}", "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture),
                Latitude = double.Parse(fields[2], CultureInfo.InvariantCulture),
                Longitude = double.Parse(fields[3], CultureInfo.InvariantCulture),
                Magnitude = double.Parse(fields[4], CultureInfo.InvariantCulture),
                IsNew = true
            }).ToList();
            #region foreach

            //var events = new List<QuakeEvent>();
            //foreach (var str in strings)
            //{
            //    var fields = str.Split(' ');
            //    events.Add(new QuakeEvent
            //    {
            //        Date = DateTime.ParseExact($"{fields[0]} {fields[1]}", "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture),
            //        Latitude = double.Parse(fields[2], CultureInfo.InvariantCulture),
            //        Longitude = double.Parse(fields[3], CultureInfo.InvariantCulture),
            //        Magnitude = double.Parse(fields[4], CultureInfo.InvariantCulture),
            //        IsNew = true
            //    });
            //}
            //return events;

            #endregion
        }
    }
}