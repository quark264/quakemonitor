namespace DatabaseModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSeismicActivity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SeismicActivities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Activity = c.Double(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.QuakeEvents", "SeismicActivityId", c => c.Int());
            CreateIndex("dbo.QuakeEvents", "SeismicActivityId");
            AddForeignKey("dbo.QuakeEvents", "SeismicActivityId", "dbo.SeismicActivities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuakeEvents", "SeismicActivityId", "dbo.SeismicActivities");
            DropIndex("dbo.QuakeEvents", new[] { "SeismicActivityId" });
            DropColumn("dbo.QuakeEvents", "SeismicActivityId");
            DropTable("dbo.SeismicActivities");
        }
    }
}
