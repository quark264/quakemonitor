namespace DatabaseModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIndexForActivities : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.SeismicActivities", new[] { "Latitude", "Longitude" }, unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.SeismicActivities", new[] { "Latitude", "Longitude" });
        }
    }
}
