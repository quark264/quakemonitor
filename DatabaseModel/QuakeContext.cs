﻿using System.Data.Entity;
using DomainEntities;

namespace DatabaseModel
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class QuakeContext : DbContext
    {
        public DbSet<QuakeEvent> QuakeEvents { get; set; }
        public DbSet<SeismicActivity> SeismicActivities { get; set; }

        public QuakeContext() : base("DefaultConnection")
        {
            
        }

        public static QuakeContext Create()
        {
            return new QuakeContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SeismicActivity>().HasMany(n => n.QuakeEvents)
                                                  .WithOptional(n => n.SeismicActivity)
                                                  .HasForeignKey(n => n.SeismicActivityId);
            modelBuilder.Entity<SeismicActivity>()
                .HasIndex(n => new {n.Latitude, n.Longitude})
                .IsUnique();
        }
    }
}