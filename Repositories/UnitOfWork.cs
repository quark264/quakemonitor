﻿using System;
using DatabaseModel;

namespace Repositories
{
    public class UnitOfWork : IDisposable
    {
        private readonly QuakeContext _context = new QuakeContext();
        private QuakeEventRepository _quakeEventRepository;
        private SeismicActivityRepository _seismicActivityRepository;

        public QuakeEventRepository QuakeEvents
        {
            get { return _quakeEventRepository ?? (_quakeEventRepository = new QuakeEventRepository(_context)); }
        }

        public SeismicActivityRepository SeismicActivities
        {
            get { return _seismicActivityRepository ?? (_seismicActivityRepository = new SeismicActivityRepository(_context)); }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}