using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseModel;
using DomainEntities;

namespace Repositories
{
    public class QuakeEventRepository : IRepository<QuakeEvent>
    {
        private readonly QuakeContext _context;

        public QuakeEventRepository(QuakeContext context)
        {
            _context = context;
        }

        public IEnumerable<QuakeEvent> GetAll()
        {
            return _context.QuakeEvents;
        }

        public QuakeEvent GetEntityBy(Func<QuakeEvent, bool> predicate)
        {
            return _context.QuakeEvents.FirstOrDefault(predicate);
        }

        public void Create(QuakeEvent entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(QuakeEvent entity)
        {
            var quakeEvent = _context.QuakeEvents.FirstOrDefault(n => n.Id == entity.Id);
            _context.Entry(quakeEvent).CurrentValues.SetValues(entity);
        }

        public void Delete(int entityId)
        {
            throw new NotImplementedException();
        }
    }
}