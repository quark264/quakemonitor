﻿using System;
using System.Collections.Generic;

namespace Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetEntityBy(Func<TEntity, bool> predicate);

        void Create(TEntity entity);

        void Edit(TEntity entity);

        void Delete(int entityId);
    }
}
