﻿using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseModel;
using DomainEntities;

namespace Repositories
{
    public class SeismicActivityRepository : IRepository<SeismicActivity>
    {
        private readonly QuakeContext _context;

        public SeismicActivityRepository(QuakeContext context)
        {
            _context = context;
        }

        public IEnumerable<SeismicActivity> GetAll()
        {
            return _context.SeismicActivities;
        }

        public SeismicActivity GetEntityBy(Func<SeismicActivity, bool> predicate)
        {
            return _context.SeismicActivities.FirstOrDefault(predicate);
        }

        public void Create(SeismicActivity entity)
        {
            _context.SeismicActivities.Add(entity);
        }

        public void Edit(SeismicActivity entity)
        {
            var seismicActivity = _context.SeismicActivities.FirstOrDefault(n => n.Id == entity.Id);
            _context.Entry(seismicActivity).CurrentValues.SetValues(entity);
        }

        public void Delete(int entityId)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            _context.SeismicActivities.RemoveRange(_context.SeismicActivities);
        }
    }
}