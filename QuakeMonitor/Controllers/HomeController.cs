﻿using System.Web.Mvc;

namespace QuakeMonitor.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Graph()
        {
            ViewBag.Title = "Graph Page";

            return View();
        }
    }
}
