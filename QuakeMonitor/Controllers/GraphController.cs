﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using QuakeMonitor.Dto;
using Repositories;

namespace QuakeMonitor.Controllers
{
    public class GraphController : ApiController
    {
        private readonly UnitOfWork _unitOfWork;

        public GraphController()
        {
            _unitOfWork = new UnitOfWork();
        }

        [HttpGet]
        [Route("Home/api/graph/GetDistributionGraph")]//make normal later
        public IEnumerable<DistributionGraphDto> GetDistributionGraph()
        {
            return _unitOfWork.QuakeEvents.GetAll()
                .GroupBy(e => e.Date.Year)
                .Select(e => new
                {
                    Year = e.Key,
                    Months = e.GroupBy(x => x.Date.ToString("MMMM"))
                        .Select(x => new
                        {
                            Name = x.Key,
                            EventsCount = x.Count(),
                            LgE = 4 + 1.8 * x.Max(o => o.Magnitude)
                        }).ToList()
                })
                .SelectMany(
                    e => e.Months,
                    (e, month) => new DistributionGraphDto
                    {
                        Year = e.Year,
                        Month = month.Name,
                        EventsCount = month.EventsCount,
                        LgE = month.LgE
                    });
        }

        [HttpGet]
        public IEnumerable<int> GetSecondGraph()
        {
            return new List<int>();
        }
    }
}