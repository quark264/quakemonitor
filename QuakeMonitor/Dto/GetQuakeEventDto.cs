﻿using System;

namespace QuakeMonitor.Dto
{
    public class GetQuakeEventDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}