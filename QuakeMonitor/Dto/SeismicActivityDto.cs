﻿namespace QuakeMonitor.Dto
{
    public class SeismicActivityDto
    {
        public int Id { get; set; }
        public double Activity { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}