﻿namespace QuakeMonitor.Dto
{
    public class DistributionGraphDto
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int EventsCount { get; set; }
        public double LgE { get; set; }
    }
}