﻿using System;

namespace QuakeMonitor.Dto
{
    public class QuakeEventDto
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Magnitude { get; set; }
    }
}