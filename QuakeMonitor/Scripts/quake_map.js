﻿var map;
var events;
var activities;
var view;
var meridianLength = 20004274;
var equatorDegree = 111.32;
var viewSpatialReference = 3995;
var centerPoint = {
    x: 1000000,
    y: -1000000,
    spatialReference: viewSpatialReference
};
var startDate = null;
var endDate = null;
var legend = null;

require([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/TileLayer",
    "esri/layers/FeatureLayer",
    "esri/layers/GraphicsLayer",
    "esri/Graphic",
    "esri/geometry/Point",
    "esri/geometry/Circle",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/geometry/SpatialReference",
    "esri/renderers/SimpleRenderer",
    "esri/renderers/HeatmapRenderer",
    "esri/tasks/GeometryService",
    "esri/tasks/support/ProjectParameters",
    "esri/widgets/LayerList",
    "esri/widgets/Legend"
],
    function (
        Map,
        MapView,
        TileLayer,
        FeatureLayer,
        GraphicsLayer,
        Graphic,
        Point,
        Circle,
        SimpleMarkerSymbol,
        SimpleFillSymbol,
        SpatialReference,
        SimpleRenderer,
        HeatmapRenderer,
        GeometryService,
        ProjectParameters,
        LayerList,
        Legend
    ) {
        map = new Map({
            layers: [
                new TileLayer({
                    url: "https://gis.ngdc.noaa.gov/arcgis/rest/services/arctic_ps/arctic_basemap/MapServer",
                    listMode: "hide"
                })
            ]
        });

        LoadQuakeData(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend, false);
        DrawGrid(Point, Circle, SimpleFillSymbol, Graphic, GraphicsLayer, GeometryService, ProjectParameters, SpatialReference);
    }
);

function latLonToXY(items, Point, GeometryService, ProjectParameters, SpatialReference) {
    var geometryService = new GeometryService("https://utility.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer");
    var params = new ProjectParameters();
    var points = [];
    $.each(items, function(index, item) {
        points.push(new Point(item.Longitude, item.Latitude));
    });
    params.geometries = points;
    params.outSpatialReference = new SpatialReference({ wkid: viewSpatialReference });
    return geometryService.project(params);
}

$(function () {
    $("#startDate").datepicker(
        {
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: "dd.mm.yy",
            onClose: function (selectedDate) {
                $("#endDate").datepicker("option", "minDate", selectedDate);
            }
        });

    $("#endDate").datepicker(
        {
            minDate: new Date(),
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            dateFormat: "dd.mm.yy",
            onClose: function (selectedDate) {
                $("#startDate").datepicker("option", "maxDate", selectedDate);
            }
        });
});

$("#filter").click(function() {
    startDate = moment.utc($("#startDate").val(), "DD.MM.YYYY", true).format();
    endDate = moment.utc($("#endDate").val(), "DD.MM.YYYY", true).format();

    var events = map.findLayerById("events");
    var eventsMag = map.findLayerById("eventsMag");
    var eventsHeat = map.findLayerById("eventsHeat");
    map.layers.removeMany([events, eventsMag, eventsHeat]);

    require([
        "esri/views/MapView",
        "esri/layers/FeatureLayer",
        "esri/Graphic",
        "esri/geometry/Point",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/geometry/SpatialReference",
        "esri/renderers/SimpleRenderer",
        "esri/renderers/HeatmapRenderer",
        "esri/tasks/GeometryService",
        "esri/tasks/support/ProjectParameters",
        "esri/widgets/LayerList",
        "esri/widgets/Legend"
    ],
        function (MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, HeatmapRenderer, GeometryService, ProjectParameters, LayerList, Legend) {
            LoadQuakeData(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend, true);
        });
});

$("#clear").click(function() {
    startDate = null;
    endDate = null;
    $("#startDate").val("");
    $("#endDate").val("");

    var events = map.findLayerById("events");
    var eventsMag = map.findLayerById("eventsMag");
    var eventsHeat = map.findLayerById("eventsHeat");
    map.layers.removeMany([events, eventsMag, eventsHeat]);

    require([
            "esri/views/MapView",
            "esri/layers/FeatureLayer",
            "esri/Graphic",
            "esri/geometry/Point",
            "esri/symbols/SimpleMarkerSymbol",
            "esri/geometry/SpatialReference",
            "esri/renderers/SimpleRenderer",
            "esri/renderers/HeatmapRenderer",
            "esri/tasks/GeometryService",
            "esri/tasks/support/ProjectParameters",
            "esri/widgets/LayerList",
            "esri/widgets/Legend"
        ],
        function (MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, HeatmapRenderer, GeometryService, ProjectParameters, LayerList, Legend) {
            LoadQuakeData(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend, true);
        });
});

function formatDate(dateString) {
    var date = dateString.split("T")[0].split("-");
    var time = dateString.split("T")[1];
    return time + " " + date[2] + "." + date[1] + "." + date[0];
}

function LoadQuakeData(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend, isFilter) {
    $.ajax({
        url: "/api/seismic/GetQuakeEvents/?startDate=" + startDate + "&endDate=" + endDate,
        type: "GET",
        dataType: "json",
        success: function (data) {
            events = data;
            DrawQuakeEvents(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend);
        },
        error: function (x, y, z) {
            alert(x + "\n" + y + "\n" + z);
        }
    });
    
    if (!isFilter) {
        $.ajax({
                url: "/api/seismic/GetSeismicActivities/",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    activities = data;
                    DrawActivities(MapView, FeatureLayer, Graphic, Point, SpatialReference, GeometryService, ProjectParameters, Legend);
                },
                error: function (x, y, z) {
                    alert(x + "\n" + y + "\n" + z);
                }
            });
    }
}

function DrawQuakeEvents(MapView, FeatureLayer, Graphic, Point, SimpleMarkerSymbol, SpatialReference, SimpleRenderer, GeometryService, ProjectParameters, LayerList, Legend) {
    var renderer = new SimpleRenderer({
        symbol: new SimpleMarkerSymbol({
            size: 10,
            color: [255, 0, 0, 0.6],
            outline: {
                color: "#000000",
                width: 0.5
            }
        })
    });

    var renderer2 = new SimpleRenderer({
        symbol: new SimpleMarkerSymbol({
            color: [255, 0, 0, 0.6],
            outline: {
                color: "#000000",
                width: 0.5
            }
        })
    });

    renderer2.visualVariables = [
        {
            type: "size",
            field: "mag",
            stops: [
                { value: 1.5, size: 1, label: "< 1.5" },
                { value: 2.5, size: 5, label: "2.5" },
                { value: 3.5, size: 8, label: "3.5" },
                { value: 4.5, size: 12, label: "4.5" },
                { value: 6.0, size: 15, label: "> 6" }
            ]
        }
    ];

    var renderer3 = {
        type: "heatmap",
        colorStops: [
            { ratio: 0, color: "rgba(255, 255, 255, 0)" },
            { ratio: 0.007, color: "rgba(50, 255, 255, 0)" },
            { ratio: 0.0075, color: "rgba(102, 255, 0, 1)" },
            { ratio: 0.0085, color: "rgba(140, 255, 0, 1)" },
            { ratio: 0.0095, color: "rgba(240, 220, 0, 1)" },
            { ratio: 0.01, color: "rgba(255, 170, 0, 1)" },
            { ratio: 0.02, color: "rgba(255, 110, 0, 1)" },
            { ratio: 0.03, color: "rgba(255, 57, 0, 1)" },
            { ratio: 0.04, color: "rgba(255, 0, 0, 1)" }
        ],
        minPixelIntensity: 0,
        maxPixelIntensity: 900
    };

    latLonToXY(events, Point, GeometryService, ProjectParameters, SpatialReference).then(function (projectedGeoms) {
        createLayers(renderer, renderer2, renderer3, projectedGeoms, MapView, FeatureLayer, Graphic, Point, SpatialReference, LayerList, Legend);
    });
}

function DrawActivities(MapView, FeatureLayer, Graphic, Point, SpatialReference, GeometryService, ProjectParameters, Legend) {
    var renderer = {
        type: "heatmap",
        colorStops: [
            { ratio: 0, color: "rgba(255, 255, 255, 0)" },
            { ratio: 0.01, color: "rgba(140, 255, 0, 1)" },
            { ratio: 0.02, color: "rgba(240, 220, 0, 1)" },
//            { ratio: 0.025, color: "rgba(255, 110, 0, 1)" },
            { ratio: 0.035, color: "rgba(255, 0, 0, 1)" }
        ],
        minPixelIntensity: 0,
        maxPixelIntensity: 700
    };
    latLonToXY(activities, Point, GeometryService, ProjectParameters, SpatialReference).then(function(projectedGeoms) {
        createActivitiesLayer(renderer, projectedGeoms, MapView, FeatureLayer, Graphic, Point, SpatialReference, Legend);
    });
}

function createLayers(renderer, renderer2, renderer3, projectedGeoms, MapView, FeatureLayer, Graphic, Point, SpatialReference, LayerList, Legend) {
    var fields = [
        {
            name: "id",
            alias: "Id",
            type: "oid"
        },
        {
            name: "latitude",
            alias: "Latitude",
            type: "double"
        },
        {
            name: "longitude",
            alias: "Longitude",
            type: "double"
        },
        {
            name: "date",
            alias: "Date",
            type: "string"
        },
        {
            name: "mag",
            alias: "Magnitude",
            type: "double"
        }
    ];

    var graphics = [];
    $.each(projectedGeoms, function(index, projectedGeom) {
        var point = new Point({
            x: projectedGeom.x,
            y: projectedGeom.y,
            spatialReference: new SpatialReference({ wkid: viewSpatialReference })
        });
        graphics.push(new Graphic({
            geometry: point,
            attributes: {
                id: index + 1,
                date: formatDate(events[index].Date),
                latitude: events[index].Latitude,
                longitude: events[index].Longitude,
                mag: events[index].Magnitude
            }
        }));
    });

    var pTemplate = {
        // autocasts as new PopupTemplate()
        title: "Event info",
        content:
            "<p>Lat: {latitude}, Lon: {longitude}</p>" +
            "<p>Date: {date}</p >" +
            "<p>M: {mag}</p>",
        fieldInfos: [
            {
                fieldName: "latitude"
            },
            {
                fieldName: "longitude"
            },
            {
                fieldName: "date"
            },
            {
                fieldName: "mag",
                format: {
                    places: 0,
                    digitSeparator: true
                }
            }
        ]
    };

    var seismicEvents = new FeatureLayer({
        id: "events",
        source: graphics,
        fields: fields,
        objectIdField: "id",
        popupTemplate: pTemplate,
        renderer: renderer,
        spatialReference: {
            wkid: viewSpatialReference
        },
        geometryType: "point",
        title: "Seismic events"
    });

    var seismicEventsWithMag = new FeatureLayer({
        id: "eventsMag",
        source: graphics,
        fields: fields,
        objectIdField: "id",
        renderer: renderer2,
        spatialReference: {
            wkid: viewSpatialReference
        },
        geometryType: "point",
        visible: false,
        title: "Seismic events with magnitude"
    });

    var seismicEventsHeatmap = new FeatureLayer({
        id: "eventsHeat",
        source: graphics,
        fields: fields,
        objectIdField: "id",
        renderer: renderer3,
        spatialReference: {
            wkid: viewSpatialReference
        },
        geometryType: "point",
        visible: false,
        title: "Density of seismic events"
    });

    map.layers.add(seismicEvents);
    map.layers.add(seismicEventsWithMag);
    map.layers.add(seismicEventsHeatmap);

    var viewOptions = {
        container: "map",
        map: map,
        viewingMode: "local",
        spatialReference: viewSpatialReference,
        scale: 12000000,
        center: centerPoint
    };
    view = new MapView(viewOptions);

    //if (legend === null) {
        legend = new Legend({
            view: view,
            layerInfos: [
                {
                    layer: seismicEventsWithMag,
                    title: "Magnitude"
                },
                //{
                //    layer: seismicEventsHeatmap,
                //    title: "Density of seismic events"
                //}
            ]
        });
        view.ui.add(legend, "bottom-right");
    //} else {
    //    legend.layerInfos.push({
    //        layer: seismicEventsWithMag,
    //        title: "Magnitude"
    //    });
    //    legend.layerInfos.push({
    //        layer: seismicEventsHeatmap,
    //        title: "Density of seismic events"
    //    });
    //}

    var layerList = new LayerList({
        view: view
    });
    view.ui.add(layerList, "top-right");
}

function createActivitiesLayer(renderer, projectedGeoms, MapView, FeatureLayer, Graphic, Point, SpatialReference, Legend) {
    var fields = [
        {
            name: "id",
            alias: "Id",
            type: "oid"
        },
        {
            name: "activity",
            alias: "Activity",
            type: "double"
        }
    ];

    var graphics = [];
    $.each(projectedGeoms, function (index, projectedGeom) {
        var point = new Point({
            x: projectedGeom.x,
            y: projectedGeom.y,
            spatialReference: new SpatialReference({ wkid: viewSpatialReference })
        });
        graphics.push(new Graphic({
            geometry: point,
            attributes: {
                id: index + 1,
                activity: activities[index].Activity
            }
        }));
    });

    var seismicActivities = new FeatureLayer({
        id: "activities",
        source: graphics,
        fields: fields,
        objectIdField: "id",
        renderer: renderer,
        spatialReference: {
            wkid: viewSpatialReference
        },
        geometryType: "point",
        visible: false,
        title: "Seismic activity"
    });

    map.layers.add(seismicActivities);
  
    //if (legend === null) {
    //    legend = new Legend({
    //        view: view,
    //        layerInfos: [
    //            {
    //                layer: seismicActivities,
    //                title: "Seismic activity"
    //            }
    //        ]
    //    });
    //    view.ui.add(legend, "bottom-right");
    //} else {
    //    legend.layerInfos.push({
    //        layer: seismicActivities,
    //        title: "Seismic activity"
    //    });
    //}
}

function DrawGrid(Point, Circle, SimpleFillSymbol, Graphic, GraphicsLayer, GeometryService, ProjectParameters, SpatialReference) {
    var graphics = [];

    /****************************
     * Create a polyline graphic
     ****************************/

    // Create a symbol for drawing the line
    var lineSymbol = {
        type: "simple-line", // autocasts as SimpleLineSymbol()
        color: [0, 0, 0],
        width: 1
    };

    for (var i = 0; i < 12; i++) {
        var polyline = {
            type: "polyline", // autocasts as new Polyline()
            paths: [[-180 + i * 15, 50], [0 + i * 15, 50]]
        };
        graphics.push(new Graphic({
            geometry: polyline,
            symbol: lineSymbol
        }));
    }

    // Create a symbol for rendering the graphic
    var fillSymbol = {
        type: "simple-fill", // autocasts as new SimpleFillSymbol()
        color: [0, 0, 0, 0],
        style: "solid",
        outline: {
            // autocasts as new SimpleLineSymbol()
            color: [0, 0, 0, 1],
            width: 1
        }
    };

    for (var j = 5; j < 9; j++) {
        var pointLat = {
            type: "point", // autocasts as new Point()
            longitude: 0,
            latitude: 10 * j
        };
        var textSymbolLat = {
            type: "text",  // autocasts as new TextSymbol()
            color: [0, 0, 0, 1],
            text: 10 * j,
            xoffset: 10,
            yoffset: 3,
            font: {  // autocast as new Font()
                size: 12,
                family: "sans-serif",
                weight: "bold"
            }
        };
        var textGraphicLat = new Graphic({
            geometry: pointLat,
            symbol: textSymbolLat
        });
        graphics.push(textGraphicLat);
    }

    for (var k = 0; k < 13; k++) {
        var pointLonE = {
            type: "point", // autocasts as new Point()
            longitude: 15 * k,
            latitude: 50
        };
        var textSymbolLonE = {
            type: "text",  // autocasts as new TextSymbol()
            color: [0, 0, 0, 1],
            text: 15 * k,
            xoffset: 10,
            yoffset: -10,
            font: {  // autocast as new Font()
                size: 12,
                family: "sans-serif",
                weight: "bold"
            }
        };
        var textGraphicLonE = new Graphic({
            geometry: pointLonE,
            symbol: textSymbolLonE
        });
        graphics.push(textGraphicLonE);
    }

    for (var l = 1; l < 12; l++) {
        var pointLonW = {
            type: "point", // autocasts as new Point()
            longitude: -15 * l,
            latitude: 50
        };
        var textSymbolLonW = {
            type: "text",  // autocasts as new TextSymbol()
            color: [0, 0, 0, 1],
            text: -15 * l,
            xoffset: 10,
            yoffset: -10,
            font: {  // autocast as new Font()
                size: 12,
                family: "sans-serif",
                weight: "bold"
            }
        };
        var textGraphicLonW = new Graphic({
            geometry: pointLonW,
            symbol: textSymbolLonW
        });
        graphics.push(textGraphicLonW);
    }

    var circleCenter = {
        Longitude: 0.0,
        Latitude: 90.0
    };
    latLonToXY([circleCenter], Point, GeometryService, ProjectParameters, SpatialReference).then(function (projectedGeoms) {
        var mapCircleCenter = new Point({
            x: projectedGeoms[0].x,
            y: projectedGeoms[0].y,
            spatialReference: new SpatialReference({ wkid: viewSpatialReference })
        });
        for (var i = 1; i < 5; i++) {
            var circle = new Circle({
                center: mapCircleCenter,
                radius: LatDegreesToKm(10 * i),
                radiusUnit: "kilometers"
            });
            var polygonGraphic = new Graphic({
                geometry: circle,
                symbol: fillSymbol
            });
            graphics.push(polygonGraphic);
        }
        var graphicLayer = new GraphicsLayer({
            id: "grid",
            graphics: graphics,
            listMode: "hide",
            spatialReference: {
                wkid: viewSpatialReference
            },
            visible: true,
            title: "Grid"
        });
        map.layers.add(graphicLayer);
    });
}

function LatDegreesToKm(lat) {
    return meridianLength * lat / (1000 * 180);
}