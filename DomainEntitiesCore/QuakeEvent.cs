﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainEntitiesCore
{
    public class QuakeEvent
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Magnitude { get; set; }
        public bool IsNew { get; set; }

        public int? SeismicActivityId { get; set; }
        [ForeignKey("SeismicActivityId")]
        public virtual SeismicActivity SeismicActivity { get; set; }
    }
}