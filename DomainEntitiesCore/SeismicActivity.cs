﻿using System.Collections.Generic;

namespace DomainEntitiesCore
{
    public class SeismicActivity
    {
        public int Id { get; set; }
        public double Activity { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public virtual ICollection<QuakeEvent> QuakeEvents { get; set; }
    }
}