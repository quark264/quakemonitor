﻿using DatabaseModelCore.Context;
using DatabaseModelCore.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;

namespace DatabaseModelCore
{
    public static class QuakeMonitorPersistanceConfigurration
    {
        public static IServiceCollection AddQuakeMonitorPersistance(this IServiceCollection collection)
        {
            var serviceProvider = collection.BuildServiceProvider();
            var configuration = serviceProvider.GetRequiredService<IQuakeMonitorDatabaseModelOptions>();

            collection.AddDbContext<QuakeMonitorContext>(o => o.UseMySQL(configuration.GetConnectionString()));

            collection.AddScoped<QuakeEventRepository>();
            collection.AddScoped<SeismicActivityRepository>();
            collection.AddScoped<UnitOfWork>();

            return collection;
        }

        public static IApplicationBuilder UseQuakeMonitorPersistance(this IApplicationBuilder builder)
        {
            using (var scopre = builder.ApplicationServices.CreateScope())
            {
                scopre.ServiceProvider.GetRequiredService<QuakeMonitorContext>().Database.Migrate();
            }

            return builder;
        }
    }
}