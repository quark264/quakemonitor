﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseModelCore.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SeismicActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Activity = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeismicActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuakeEvents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:AutoIncrement", true),
                    Date = table.Column<DateTime>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Magnitude = table.Column<double>(nullable: false),
                    IsNew = table.Column<short>(nullable: false),
                    SeismicActivityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuakeEvents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuakeEvents_SeismicActivities_SeismicActivityId",
                        column: x => x.SeismicActivityId,
                        principalTable: "SeismicActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuakeEvents_SeismicActivityId",
                table: "QuakeEvents",
                column: "SeismicActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_SeismicActivities_Latitude_Longitude",
                table: "SeismicActivities",
                columns: new[] { "Latitude", "Longitude" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuakeEvents");

            migrationBuilder.DropTable(
                name: "SeismicActivities");
        }
    }
}
