﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DatabaseModelCore.Context
{
    public sealed class DesignTimeQuakeMonitorDbContextFactory : IDesignTimeDbContextFactory<QuakeMonitorContext>

    {
        public QuakeMonitorContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<QuakeMonitorContext>();

            builder.UseMySQL("server=localhost;UserId=admin;Password=Alexey0426;database=QuakeMonitor;CharSet=utf8;Persist Security Info=True");

            return new QuakeMonitorContext(builder.Options);
        }
    }
}