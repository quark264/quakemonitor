﻿using DomainEntitiesCore;
using Microsoft.EntityFrameworkCore;

namespace DatabaseModelCore.Context
{
    public sealed class QuakeMonitorContext : DbContext
    {
        public DbSet<QuakeEvent> QuakeEvents { get; set; }

        public DbSet<SeismicActivity> SeismicActivities { get; set; }

        public QuakeMonitorContext(DbContextOptions<QuakeMonitorContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<QuakeEvent>(e =>
            {
                e.HasOne(x => x.SeismicActivity)
                    .WithMany(x => x.QuakeEvents)
                    .HasForeignKey(x => x.SeismicActivityId);
                e.Property(p => p.IsNew)
                    .HasConversion<int>();
            });

            builder.Entity<SeismicActivity>()
                .HasIndex(n => new { n.Latitude, n.Longitude })
                .IsUnique();
        }
    }
}