﻿namespace DatabaseModelCore
{
    public interface IQuakeMonitorDatabaseModelOptions
    {
        string GetConnectionString();
    }
}