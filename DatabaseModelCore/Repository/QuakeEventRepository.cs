using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseModelCore.Context;
using DomainEntitiesCore;

namespace DatabaseModelCore.Repository
{
    public class QuakeEventRepository : IRepository<QuakeEvent>
    {
        private readonly QuakeMonitorContext _�ontext;

        public QuakeEventRepository(QuakeMonitorContext �ontext)
        {
            _�ontext = �ontext;
        }

        public IEnumerable<QuakeEvent> GetAll()
        {
            return _�ontext.QuakeEvents;
        }

        public QuakeEvent GetEntityBy(Func<QuakeEvent, bool> predicate)
        {
            return _�ontext.QuakeEvents.FirstOrDefault(predicate);
        }

        public void Create(QuakeEvent entity)
        {
            throw new NotImplementedException();
        }

        public void Edit(QuakeEvent entity)
        {
            var quakeEvent = _�ontext.QuakeEvents.FirstOrDefault(n => n.Id == entity.Id);
            _�ontext.Entry(quakeEvent ?? throw new NullReferenceException()).CurrentValues.SetValues(entity);
        }

        public void Delete(int entityId)
        {
            throw new NotImplementedException();
        }
    }
}