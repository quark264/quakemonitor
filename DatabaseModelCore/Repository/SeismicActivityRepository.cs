﻿using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseModelCore.Context;
using DomainEntitiesCore;

namespace DatabaseModelCore.Repository
{
    public class SeismicActivityRepository : IRepository<SeismicActivity>
    {
        private readonly QuakeMonitorContext _сontext;

        public SeismicActivityRepository(QuakeMonitorContext сontext)
        {
            _сontext = сontext;
        }

        public IEnumerable<SeismicActivity> GetAll()
        {
            return _сontext.SeismicActivities;
        }

        public SeismicActivity GetEntityBy(Func<SeismicActivity, bool> predicate)
        {
            return _сontext.SeismicActivities.FirstOrDefault(predicate);
        }

        public void Create(SeismicActivity entity)
        {
            _сontext.SeismicActivities.Add(entity);
        }

        public void Edit(SeismicActivity entity)
        {
            var seismicActivity = _сontext.SeismicActivities.FirstOrDefault(n => n.Id == entity.Id);
            _сontext.Entry(seismicActivity ?? throw new NullReferenceException()).CurrentValues.SetValues(entity);
        }

        public void Delete(int entityId)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            _сontext.SeismicActivities.RemoveRange(_сontext.SeismicActivities);
        }
    }
}