﻿using System;
using DatabaseModelCore.Context;

namespace DatabaseModelCore.Repository
{
    public class UnitOfWork : IDisposable
    {
        private readonly QuakeMonitorContext _сontext;

        public QuakeEventRepository QuakeEvents { get; }

        public SeismicActivityRepository SeismicActivities { get; }

        public UnitOfWork(
            QuakeMonitorContext сontext,
            QuakeEventRepository quakeEventRepository,
            SeismicActivityRepository seismicActivityRepository
        )
        {
            _сontext = сontext;
            QuakeEvents = quakeEventRepository;
            SeismicActivities = seismicActivityRepository;
        }

        public void Save()
        {
            _сontext.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _сontext.Dispose();
                }

                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}