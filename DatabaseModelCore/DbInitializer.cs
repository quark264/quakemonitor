﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using DatabaseModelCore.Context;
using DomainEntitiesCore;

namespace DatabaseModelCore
{
    public static class DbInitializer
    {
        public static void InitializeDb(QuakeMonitorContext context)
        {
            var path = $"{Directory.GetCurrentDirectory()}\\землетрясения+.txt";
            //const string path = "G:\\Documents\\Visual Studio 2017\\Projects\\QuakeMonitor\\землетрясения+.txt";
            //const string path = "C:\\Users\\Tom\\Documents\\Visual Studio 2017\\Projects\\QuakeMonitor\\землетрясения+.txt";
            context.QuakeEvents.AddRange(ParseEventsFromFile(path));
        }

        private static IEnumerable<QuakeEvent> ParseEventsFromFile(string path)
        {
            var strr = new StreamReader(new FileStream(path, FileMode.Open));
            var result = strr.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var strings = result.Skip(1).Take(result.Length - 1).ToArray();
            return strings.Select(str => str.Split(' ')).Select(fields => new QuakeEvent
            {
                Date = DateTime.ParseExact($"{fields[0]} {fields[1]}", "dd.MM.yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture),
                Latitude = double.Parse(fields[2], CultureInfo.InvariantCulture),
                Longitude = double.Parse(fields[3], CultureInfo.InvariantCulture),
                Magnitude = double.Parse(fields[4], CultureInfo.InvariantCulture),
                IsNew = true
            }).ToList();
        }
    }
}